#!/bin/sh

# treat undefined variables as an error, and exit script immediately on error
set -eux
MAX_WAIT_SECONDS="$1"

# hit the status endpoint (don't kill script on failure)
set +e

docker inspect 'myhelloworld'
docker inspect 'db'

result= 

while [ "$result" != "200" ]
do
	sleep 1.0
	result=$(curl -s -o /dev/null -w "%{http_code}" http://docker:5002/healthcheck)
	
	MAX_WAIT_SECONDS=$((MAX_WAIT_SECONDS-1))

	if [ $MAX_WAIT_SECONDS -le 0 ]; then
        echo "Docker smoke test failed"
        docker kill 'myhelloworld' || true
		docker kill 'db' || true
        exit -1
    fi
done

# Reenable exit on error
set -e

if [ $result -eq 200 ]; then
   exit 0
else
   exit 1
fi
