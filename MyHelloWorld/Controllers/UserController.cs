﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MyHelloWorld.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        [HttpGet]
        public async Task<ActionResult> Get(string name, string password)
        {


            if (string.IsNullOrEmpty(name)) 
                throw new ArgumentException("Value cannot be null or empty.", nameof(name));
            if (string.IsNullOrEmpty(password))
                throw new ArgumentException("Value cannot be null or empty.", nameof(password));

            string qry = "select * from User where name='" + name + "'and Password='" + password + "' ";
            string user;

            using (SqlConnection c = new SqlConnection(""))
            {
                SqlCommand cmd = new SqlCommand(qry)
                {
                    Connection = c
                };
                SqlDataReader r = cmd.ExecuteReader();

                var b = await r.ReadAsync();
                user = r.GetString(0);

            }

            return Ok(user);
        }
    }
}
